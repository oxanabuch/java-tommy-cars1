package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car {

    private double bodyCapacity;


    public Lorry(String brand, String xclass, double weight, Driver driver, Engine engine, int i) {
        super(brand, xclass, weight, driver, engine);
    }

    public double getBodyCapacity() {
        return bodyCapacity;
    }

    public void setBodyCapacity(double bodyCapacity) {
        this.bodyCapacity = bodyCapacity;
    }

    @Override
    public void carGo() {
        System.out.println("Let`s go not fast!");
    }

    @Override
    public void carStop() {
        System.out.println("Stop near that building!");
    }

}
