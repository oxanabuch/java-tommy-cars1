import com.company.details.Engine;
import com.company.professions.Driver;
import com.company.vehicles.Car;
import com.company.vehicles.Lorry;
import com.company.vehicles.SportCar;

public class Main {

    public static void main(String[] args) {

        Driver driver1 = new Driver("Jonson", 15);
        Engine engine1 = new Engine("500", "Audi");
        Car car = new Car("Audi", "Q", 2800, driver1, engine1);
        car.carGo();
        car.carStop();
        car.turnLeft();
        car.turnRight();
        System.out.println("----------");

        Driver driver2 = new Driver("Dizel", 25);
        Engine engine2 = new Engine("100", "MAN");
        Lorry lorry = new Lorry("MAN", "S", 8000, driver2, engine2, 10000);
        lorry.carGo();
        lorry.carStop();
        lorry.turnLeft();
        lorry.turnRight();
        System.out.println("----------");

        Driver driver3 = new Driver("Tomas", 45);
        Engine engine3 = new Engine("600", "Ferrari");
        SportCar sportCar = new SportCar("Ferrari", "SF", 2500, driver3, engine3, 350);
        sportCar.carGo();
        sportCar.carStop();
        sportCar.turnLeft();
        sportCar.turnRight();
        System.out.println("----------");

        System.out.println(car);
        System.out.println(lorry);
        System.out.println(sportCar);
    }
}
