package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car {

    private double speed;


    public SportCar(String brand, String xclass, double weight, Driver driver, Engine engine, int i) {
        super(brand, xclass, weight, driver, engine);
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    @Override
    public void carGo() {
        System.out.println("Let`s go very fast!");
    }

    @Override
    public void carStop() {
        System.out.println("Stop right now!");
    }

}
